package main

import (
	"net/http"
	//"path/filepath"
	//"html/template"
)

func main() {
	mux := http.NewServeMux()
	files := http.FileServer(http.Dir("public"))
	mux.Handle("/static/", http.StripPrefix("/static/", files))

	//mux.HandleFunc("/hello", hello)

	mux.HandleFunc("/", index)
	mux.HandleFunc("/err", err)

	mux.HandleFunc("/login", login)
	mux.HandleFunc("/logout", logout)
	mux.HandleFunc("/signup", signup)
	mux.HandleFunc("/signup_account", signupAccount)
	mux.HandleFunc("/authenticate", authenticate)

	mux.HandleFunc("/thread/new", newThread)
	mux.HandleFunc("/thread/create", createThread)
	mux.HandleFunc("/thread/post", postThread)
	mux.HandleFunc("/thread/read", readThread)

	server := &http.Server{
		Addr:    "0.0.0.0:8080",
		Handler: mux,
	}

	server.ListenAndServe()
}

/*
func hello(w http.ResponseWriter, req *http.Request) {
	layout := filepath.Join("templates", "login.layout.html")
	content := filepath.Join("templates", "login.html")

	tmpl, ok := template.ParseFiles(layout, content)
	if nil!=ok {
		http.NotFound(w,req)
		return
	}

	tmpl.ExecuteTemplate(w,"layout", nil)
}
*/
