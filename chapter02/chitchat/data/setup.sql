drop table posts;
drop table threads;
drop table sessions;
drop table users;

create table users (
  id         int auto_increment,
  uuid       varchar(64) not null unique,
  name       varchar(255),
  email      varchar(255) not null unique,
  password   varchar(255) not null,
  created_at timestamp not null,   

  PRIMARY KEY (id)
);

create table sessions (
  id         int auto_increment,
  uuid       varchar(64) not null unique,
  email      varchar(255),
  user_id    int,
  created_at timestamp not null,

  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

create table threads (
  id         int auto_increment,
  uuid       varchar(64) not null unique,
  topic      text,
  user_id    int,
  created_at timestamp not null,       

  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

create table posts (
  id         int,
  uuid       varchar(64) not null unique,
  body       text,
  user_id    int,
  thread_id  int,
  created_at timestamp not null,

  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (thread_id) REFERENCES threads(id)

);
