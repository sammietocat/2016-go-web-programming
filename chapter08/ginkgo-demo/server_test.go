package main

import (
	"encoding/json"
	. "github.com/onsi/ginkgo"
	"net/http"
	"net/http/httptest"
)

var _ = Describe("Testing with Ginkgo", func() {
	It("handle get", func() {

		mux := http.NewServeMux()
		mux.HandleFunc("/post/", handleRequest(&FakePost{}))

		writer := httptest.NewRecorder()

		request, _ := http.NewRequest("GET", "/post/2", nil)
		mux.ServeHTTP(writer, request)

		if 200 != writer.Code {
			GinkgoT().Errorf("Response code is %v", writer.Code)
		}

		var post Post
		json.Unmarshal(writer.Body.Bytes(), &post)
		if 2 != post.Id {
			GinkgoT().Error("Cannot retrieve JSON post")
		}
	})
})

type FakePost struct {
	Id      int
	Content string
	AUthor  string
}

func (post *FakePost) fetch(id int) (err error) {
	post.Id = id
	return
}
func (post *FakePost) create() (err error) {
	return
}
func (post *FakePost) update() (err error) {
	return
}
func (post *FakePost) delete() (err error) {
	return
}
