package main

import "database/sql"

type Text interface {
	fetch(id int) (err error)
	create() (err error)
	update() (err error)
	delete() (err error)
}

type Post struct {
	Db      *sql.DB
	Id      int    `json:"id"`
	Content string `json:"content"`
	Author  string `json:"author"`
}

/* implement Text by Post */
func (post *Post) fetch(id int) (err error) {
	err = post.Db.QueryRow("select id, content, author from posts where id = ?", id).Scan(&post.Id, &post.Content, &post.Author)
	return
}

func (post *Post) create() (err error) {
	statement := "insert into posts (content, author) values (?,?)"
	stmt, err := post.Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(post.Content, post.Author)
	if nil != err {
		return
	}

	err = post.Db.QueryRow("SELECT LAST_INSERT_ID()").Scan(&post.Id)
	return
}

func (post *Post) update() (err error) {
	_, err = post.Db.Exec("update posts set content = ?, author = ? where id = ?", post.Content, post.Author, post.Id)
	return
}

func (post *Post) delete() (err error) {
	_, err = post.Db.Exec("delete from posts where id = ?", post.Id)
	return
}

/* implement Text by Post */
