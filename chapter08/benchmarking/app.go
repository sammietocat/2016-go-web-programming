package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type Post struct {
	Id       int       `json:"id"`
	Content  string    `json:"content"`
	Author   Author    `json:"author"`
	Comments []Comment `json:"comments"`
}

type Author struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type Comment struct {
	Id      int    `json:"id"`
	Content string `json:"content"`
	Author  string `json:"author"`
}

func decode(path string) (post Post, err error) {
	in, err := os.Open(path)
	if nil != err {
		fmt.Println("Error opening JSON file:", err)
		return
	}
	defer in.Close()

	decoder := json.NewDecoder(in)
	err = decoder.Decode(&post)
	if nil != err {
		fmt.Println("Error decoding JSON:", err)
		return
	}

	return
}

func unmarshal(path string) (post Post, err error) {
	in, err := os.Open(path)
	if nil != err {
		fmt.Println("Error opening JSON file:", err)
		return
	}
	defer in.Close()

	inData, err := ioutil.ReadAll(in)
	if nil != err {
		fmt.Println("Error reading JSON data:", err)
		return
	}

	json.Unmarshal(inData, &post)
	return
}

func main() {
	post, err := decode("post.json")
	if nil != err {
		fmt.Println("Error:", err)
	}

	fmt.Println(post)
}
