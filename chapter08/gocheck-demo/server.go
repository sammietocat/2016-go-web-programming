package main

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"path"
	"strconv"

	"github.com/go-sql-driver/mysql"
)

func main() {
	cfg := mysql.Config{
		User:      "sammy",
		Passwd:    "hello",
		Net:       "tcp",
		Addr:      "localhost:3306",
		DBName:    "gwp",
		ParseTime: true,
	}

	db, err := sql.Open("mysql", cfg.FormatDSN())
	if nil != err {
		panic(err)
	}

	err = db.Ping()
	if nil != err {
		panic(err)
	}
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/post/", handleRequest(&Post{Db: db}))
	server.ListenAndServe()
}

func handleRequest(text Text) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		var err error
		switch req.Method {
		case "GET":
			err = handleGet(w, req, text)
		case "POST":
			err = handlePost(w, req, text)
		case "PUT":
			err = handlePut(w, req, text)
		case "DELETE":
			err = handleDelete(w, req, text)
		}

		if nil != err {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}
func handleGet(w http.ResponseWriter, r *http.Request, post Text) (err error) {
	id, err := strconv.Atoi(path.Base(r.URL.Path))
	if err != nil {
		return
	}
	err = post.fetch(id)
	if err != nil {
		return
	}
	output, err := json.MarshalIndent(&post, "", "\t\t")
	if err != nil {
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(output)
	// for testing check.v1
	http.NotFound(w, r)
	return
}
func handlePost(w http.ResponseWriter, r *http.Request, post Text) (err error) {
	len := r.ContentLength
	body := make([]byte, len)
	r.Body.Read(body)
	json.Unmarshal(body, &post)
	err = post.create()
	if err != nil {
		return
	}
	w.WriteHeader(200)
	return
}
func handlePut(w http.ResponseWriter, r *http.Request, post Text) (err error) {
	id, err := strconv.Atoi(path.Base(r.URL.Path))
	if err != nil {
		return
	}
	err = post.fetch(id)
	if err != nil {
		return
	}
	len := r.ContentLength
	body := make([]byte, len)
	r.Body.Read(body)
	json.Unmarshal(body, &post)

	err = post.update()
	if err != nil {
		return
	}
	w.WriteHeader(200)
	return
}
func handleDelete(w http.ResponseWriter, r *http.Request, post Text) (err error) {
	id, err := strconv.Atoi(path.Base(r.URL.Path))
	if err != nil {
		return
	}
	err = post.fetch(id)
	if err != nil {
		return
	}
	err = post.delete()
	if err != nil {
		return
	}
	w.WriteHeader(200)
	return
}
