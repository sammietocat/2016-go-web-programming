package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	. "gopkg.in/check.v1"
)

type PostTestSuite struct{}

func init() {
	Suite(&PostTestSuite{})
}

func Test(t *testing.T) { TestingT(t) }

type FakePost struct {
	Id      int
	Content string
	AUthor  string
}

func (post *FakePost) fetch(id int) (err error) {
	post.Id = id
	return
}
func (post *FakePost) create() (err error) {
	return
}
func (post *FakePost) update() (err error) {
	return
}
func (post *FakePost) delete() (err error) {
	return
}

func (s *PostTestSuite) TestHandleGet(c *C) {
	mux := http.NewServeMux()
	mux.HandleFunc("/post/", handleRequest(&FakePost{}))

	writer := httptest.NewRecorder()
	// get post indexed by 2
	request, _ := http.NewRequest("GET", "/post/2", nil)
	mux.ServeHTTP(writer, request)

	c.Check(writer.Code, Equals, 200)

	var post Post
	json.Unmarshal(writer.Body.Bytes(), &post)
	c.Check(post.Id, Equals, 2)
}
