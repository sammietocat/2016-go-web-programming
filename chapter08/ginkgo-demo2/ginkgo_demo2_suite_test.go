package main_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"testing"
)

func TestGinkgoDemo2(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "GinkgoDemo2 Suite")
}
