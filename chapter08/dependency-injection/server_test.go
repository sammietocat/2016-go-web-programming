package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

type FakePost struct {
	Id      int
	Content string
	AUthor  string
}

func (post *FakePost) fetch(id int) (err error) {
	post.Id = id
	return
}
func (post *FakePost) create() (err error) {
	return
}
func (post *FakePost) update() (err error) {
	return
}
func (post *FakePost) delete() (err error) {
	return
}

func TestHandleGet(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/post/", handleRequest(&FakePost{}))

	writer := httptest.NewRecorder()
	// get post indexed by 2
	request, _ := http.NewRequest("GET", "/post/2", nil)
	mux.ServeHTTP(writer, request)

	if 200 != writer.Code {
		t.Errorf("Response code is %v", writer.Code)
	}

	var post Post
	json.Unmarshal(writer.Body.Bytes(), &post)
	if 2 != post.Id {
		t.Error("Cannot retrieve JSON post")
	}
}

/*
func TestHandlePut(t *testing.T) {
	mux := http.NewServeMux()
	mux.HandleFunc("/post/", handleRequest)

	writer := httptest.NewRecorder()
	inJson := strings.NewReader(`{"content":"Updated post","author":"Sau Sheong"}`)
	request, _ := http.NewRequest("PUT", "/post/2", inJson)
	mux.ServeHTTP(writer, request)

	if 200 != writer.Code {
		t.Errorf("Response code is %v", writer.Code)
	}
}
*/
