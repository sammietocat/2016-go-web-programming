drop table posts;

create table posts (
  id      int auto_increment,
  content text,
  author  varchar(255),

  PRIMARY KEY(id)
);
