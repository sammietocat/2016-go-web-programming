package main

import (
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func hello(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
	fmt.Fprintf(w, "Hello, %s\n", p.ByName("name"))
}
func world(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
	fmt.Fprintf(w, "Hello World\n")
}

func main() {
	mux := httprouter.New()
	// access in URL like "/hello/blabla", where "blabla" is assigned name as "name"
	mux.GET("/hello/:name", hello)
	mux.GET("/world", world)

	server := http.Server{
		Addr:    "127.0.0.1:8080",
		Handler: mux,
	}

	server.ListenAndServe()
}
