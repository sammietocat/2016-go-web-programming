package main

import (
	"fmt"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

type Post struct {
	Id        int
	Content   string
	Author    string `sql:"not null"`
	Comments  []Comment
	CreatedAt time.Time
}

type Comment struct {
	Id        int
	Content   string
	Author    string `sql:"not null"`
	PostId    int    `sql:"index"`
	CreatedAt time.Time
}

var Db *gorm.DB

func init() {
	var err error

	cfg := mysql.Config{
		User:      "sammy",
		Passwd:    "hello",
		Net:       "tcp",
		Addr:      "localhost:3306",
		DBName:    "gwp",
		ParseTime: true,
	}

	Db, err = gorm.Open("mysql", cfg.FormatDSN())
	if nil != err {
		panic(err)
	}

	Db.AutoMigrate(&Post{}, &Comment{})
}

func main() {
	//post := Post{Content: "Hello World!", Author: "Sau Sheong"}
	post := Post{Content: "Hello World!", Author: "Joe"}
	fmt.Println(post)

	Db.Create(&post)
	fmt.Println(post)

	comment := Comment{Content: "Good post!", Author: "Joe"}
	Db.Model(&post).Association("Comments").Append(comment)

	var readPost Post
	Db.Where("author = ?", "Joe").First(&readPost)
	fmt.Println(readPost)

	var comments []Comment
	Db.Model(&readPost).Related(&comments)

	fmt.Println(comments[0])
}
