package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"io/ioutil"
)

type Post struct {
	Id      int
	Content string
	Author  string
}

func store(data interface{}, filename string) {
	buf := new(bytes.Buffer)
	encoder := gob.NewEncoder(buf)
	err := encoder.Encode(data)
	if nil != err {
		panic(err)
	}

	err = ioutil.WriteFile(filename, buf.Bytes(), 0600)
	if nil != err {
		panic(err)
	}
}

func load(data interface{}, filename string) {
	raw, err := ioutil.ReadFile(filename)
	if nil != err {
		panic(err)
	}

	buf := bytes.NewBuffer(raw)
	decoder := gob.NewDecoder(buf)
	err = decoder.Decode(data)

	if nil != err {
		panic(err)
	}
}

func main() {
	post := Post{Id: 1, Content: "Hello World!", Author: "Sau Sheong"}
	store(post, "post1")

	var postRead Post
	load(&postRead, "post1")
	fmt.Println(postRead)
}
