package main

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

type Post struct {
	Id      int
	Content string
	Author  string
}

var Db *sql.DB

func init() {
	var err error

	cfg := mysql.Config{
		User:      "sammy",
		Passwd:    "hello",
		Net:       "tcp",
		Addr:      "localhost:3306",
		DBName:    "gwp",
		ParseTime: true,
	}

	Db, err = sql.Open("mysql", cfg.FormatDSN())
	if nil != err {
		panic(err)
	}

	err = Db.Ping()
	if nil != err {
		panic(err)
	}
}

func Posts(limit int) (posts []Post, err error) {
	rows, err := Db.Query("select id, content, author from posts limit ?", limit)
	if err != nil {
		return
	}
	for rows.Next() {
		post := Post{}
		err = rows.Scan(&post.Id, &post.Content, &post.Author)
		if err != nil {
			return
		}
		posts = append(posts, post)
	}
	rows.Close()
	return
}

func GetPost(id int) (post Post, err error) {
	post = Post{}
	err = Db.QueryRow("select id, content, author from posts where id = ?", id).Scan(&post.Id, &post.Content, &post.Author)
	return
}

func (post *Post) Create() (err error) {
	statement := "insert into posts (content, author) values (?,?)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(post.Content, post.Author)
	if nil != err {
		return
	}

	err = Db.QueryRow("SELECT LAST_INSERT_ID()").Scan(&post.Id)
	//err = Db.Query("select id from posts WHERE content=? AND author=?", post.Content, post.Author).Scan(&post.Id)

	return
}

func (post *Post) Update() (err error) {
	_, err = Db.Exec("update posts set content = ?, author = ? where id = ?", post.Id, post.Content, post.Author)
	return
}

func (post *Post) Delete() (err error) {
	_, err = Db.Exec("delete from posts where id = ?", post.Id)
	return
}

func main() {
	post := Post{Content: "Hello World!", Author: "Sau Sheong"}

	fmt.Println(post)
	err := post.Create()
	if nil != err {
		fmt.Println("Create(): ", err.Error())
	}
	fmt.Println(post)

	readPost, _ := GetPost(post.Id)
	fmt.Println(readPost)

	readPost.Content = "Bonjour Monde!"
	readPost.Author = "Pierre"
	readPost.Update()

	posts, _ := Posts(2)
	fmt.Println(posts)

	readPost.Delete()
}
