drop table posts cascade if exists;
drop table comments;

create table posts (
  id      int auto_increment,
  content text,
  author  varchar(255),

  PRIMARY KEY (id)
);

create table comments (
  id      int AUTO_INCREMENT,
  content text,
  author  varchar(255),
  post_id int,

  PRIMARY KEY (id),
  FOREIGN KEY (post_id) REFERENCES posts(id)
);
