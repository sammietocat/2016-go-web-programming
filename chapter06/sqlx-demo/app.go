package main

import (
	"fmt"

	"github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type Post struct {
	Id         int
	Content    string
	AuthorName string `db: author`
}

var Db *sqlx.DB

func init() {
	cfg := mysql.Config{
		User:      "sammy",
		Passwd:    "hello",
		Net:       "tcp",
		Addr:      "localhost:3306",
		DBName:    "gwp",
		ParseTime: true,
	}

	var err error
	Db, err = sqlx.Open("mysql", cfg.FormatDSN())
	if err != nil {
		panic(err)
	}
}

func GetPost(id int) (post Post, err error) {
	post = Post{}
	err = Db.QueryRowx("select id, content, author from posts where id = ?", id).StructScan(&post)
	if err != nil {
		return
	}
	return
}

func (post *Post) Create() (err error) {
	_, err = Db.Exec("insert into posts (content, author) values (?, ?)", post.Content, post.AuthorName)
	if nil != err {
		return
	}

	err = Db.QueryRow("SELECT LAST_INSERT_ID()").Scan(&post.Id)
	return
}

func main() {
	post := Post{Content: "Hello World!", AuthorName: "Sau Sheong"}
	post.Create()
	fmt.Println(post)
}
