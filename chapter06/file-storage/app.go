package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	data := []byte("Hello World!")
	err := ioutil.WriteFile("data1", data, 0644)
	if nil != err {
		panic(err.Error())
	}

	read1, _ := ioutil.ReadFile("data1")
	fmt.Println(string(read1))

	file1, _ := os.Create("data2")
	defer file1.Close()

	bytes, _ := file1.Write(data)
	fmt.Printf("Wrote %d bytes to data2\n", bytes)

	file2, _ := os.Open("data2")
	defer file2.Close()

	read2 := make([]byte, len(data))
	bytes, _ = file2.Read(read2)
	fmt.Printf("Read %d bytes from data2\n", bytes)
	fmt.Println(string(read2))
}
