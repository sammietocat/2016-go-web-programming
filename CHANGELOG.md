# Change Log

## [2017-08-12]  
+ **Added**  
	- `README.md`  
	- `CHANGELOG.md`
	- all listings for chapter 01  
	- half way down chapter 02  
	- add the source codes (resides in `gwp` folder) offered by the author  
