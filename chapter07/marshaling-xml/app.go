package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
)

type Post struct {
	XMLName xml.Name `xml:"post"`
	Id      string   `xml:"id,attr"`
	Content string   `xml:"content"`
	Author  Author   `xml:"author"`
}

type Author struct {
	Id   string `xml:"id,attr"`
	Name string `xml:",chardata"`
}

func main() {
	post := Post{
		Id:      "1",
		Content: " Hello World!",
		Author: Author{
			Id:   "2",
			Name: "Sau Sheong",
		},
	}

	// no pretty
	//out, err := xml.Marshal(&post)
	// pretty
	out, err := xml.MarshalIndent(&post, "", "\t")
	if nil != err {
		fmt.Println("Error marshalling to XML: ", err)
		return
	}

	// no header
	//err = ioutil.WriteFile("post.xml", out, 0644)
	err = ioutil.WriteFile("post.xml", []byte(xml.Header+string(out)), 0644)
	if nil != err {
		fmt.Println("Error writing XML to file:", err)
		return
	}
}
