package main

import (
	"database/sql"

	"github.com/go-sql-driver/mysql"
)

var Db *sql.DB

func init() {
	var err error

	cfg := mysql.Config{
		User:      "sammy",
		Passwd:    "hello",
		Net:       "tcp",
		Addr:      "localhost:3306",
		DBName:    "gwp",
		ParseTime: true,
	}

	Db, err = sql.Open("mysql", cfg.FormatDSN())
	if nil != err {
		panic(err)
	}

	err = Db.Ping()
	if nil != err {
		panic(err)
	}
}

func retrieve(id int) (post Post, err error) {
	post = Post{}
	err = Db.QueryRow("select id, content, author from posts where id = ?", id).Scan(&post.Id, &post.Content, &post.Author)
	return
}

func (post *Post) create() (err error) {
	statement := "insert into posts (content, author) values (?,?)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(post.Content, post.Author)
	if nil != err {
		return
	}

	err = Db.QueryRow("SELECT LAST_INSERT_ID()").Scan(&post.Id)
	return
}

func (post *Post) update() (err error) {
	_, err = Db.Exec("update posts set content = ?, author = ? where id = ?", post.Content, post.Author, post.Id)
	return
}

func (post *Post) delete() (err error) {
	_, err = Db.Exec("delete from posts where id = ?", post.Id)
	return
}
