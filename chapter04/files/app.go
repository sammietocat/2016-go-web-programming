package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func process(w http.ResponseWriter, req *http.Request) {
	/*
		req.ParseMultipartForm(1024)
		fileHeader := req.MultipartForm.File["upload"][0]
		file, err := fileHeader.Open()
	*/
	file, _, err := req.FormFile("upload")
	if nil == err {
		data, err := ioutil.ReadAll(file)
		if nil == err {
			fmt.Fprintln(w, string(data))
		}
	}
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)
	server.ListenAndServe()
}
