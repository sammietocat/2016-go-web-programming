package main

import (
	"fmt"
	"net/http"
)

func body(w http.ResponseWriter, req *http.Request) {
	len := req.ContentLength
	body := make([]byte, len)

	req.Body.Read(body)

	fmt.Fprintln(w, string(body))
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/body", body)
	server.ListenAndServe()
}
