package main

import (
	"fmt"
	"net/http"
)

func process(w http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	fmt.Fprintln(w, req.Form)
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)
	server.ListenAndServe()
}
