package main

import (
	"fmt"
	"net/http"
)

func setCookie(w http.ResponseWriter, req *http.Request) {
	c1 := http.Cookie{
		Name:     "first_cookie",
		Value:    "Go Web Programming",
		HttpOnly: true,
	}

	c2 := http.Cookie{
		Name:     "second_cookie",
		Value:    "SJTU",
		HttpOnly: true,
	}

	/*
		w.Header().Set("Set-Cookie", c1.String())
		w.Header().Add("Set-Cookie", c2.String())
	*/
	http.SetCookie(w, &c1)
	http.SetCookie(w, &c2)
}
func getCookie(w http.ResponseWriter, req *http.Request) {
	h := req.Header["Cookie"]
	fmt.Fprintln(w, h)

	/*
		c1, err := req.Cookie("first_cookie")
		if nil != err {
			fmt.Fprintln(w, "Cannot get the 1st cookie")
		}

		cs := req.Cookies()
		fmt.Fprintln(w, c1)
		fmt.Fprintln(w, cs)
	*/
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/set_cookie", setCookie)
	http.HandleFunc("/get_cookie", getCookie)
	server.ListenAndServe()
}
