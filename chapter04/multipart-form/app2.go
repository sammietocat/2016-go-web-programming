package main

import (
	"fmt"
	"net/http"
)

func process(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(w, "(1)", req.FormValue("hello"))
	fmt.Fprintln(w, "(2)", req.PostFormValue("hello"))
	fmt.Fprintln(w, "(3)", req.PostForm)
	fmt.Fprintln(w, "(4)", req.MultipartForm)
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)
	server.ListenAndServe()
}
