package main

import (
	"fmt"
	"net/http"
)

func process(w http.ResponseWriter, req *http.Request) {
	//req.ParseForm()
	req.ParseMultipartForm(1024)
	fmt.Fprintln(w, req.MultipartForm)
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)
	server.ListenAndServe()
}
