package main

import (
	"html/template"
	"net/http"
)

func process(w http.ResponseWriter, req *http.Request) {
	tmpl, _ := template.ParseFiles("tmpl.html")
	dayOfWeek := []string{"Mon", "Tues", "Wed", "Thur", "Fri", "Sat", "Sun"}
	tmpl.Execute(w, dayOfWeek)
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)

	server.ListenAndServe()
}
