package main

import "net/http"
import "html/template"

func process(w http.ResponseWriter, req *http.Request) {
	tmplFile := `<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Go Web Programming</title>
</head>
<body>
{{ . }}
</body>
</html>
`
	tmpl := template.New("tmpl")
	tmpl, _ = tmpl.Parse(tmplFile)
	tmpl.Execute(w, "Hello World!")
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)

	server.ListenAndServe()
}
