package main

import (
	"html/template"
	"math/rand"
	"net/http"
	"time"
)

func process(w http.ResponseWriter, req *http.Request) {
	tmpl, _ := template.ParseFiles("layout.html")
	tmpl.ExecuteTemplate(w, "layout", "")
}

func process2(w http.ResponseWriter, req *http.Request) {
	rand.Seed(time.Now().Unix())
	var tmpl *template.Template
	if rand.Intn(10) > 5 {
		tmpl, _ = template.ParseFiles("layout.html", "red_hello.html")
	} else {
		tmpl, _ = template.ParseFiles("layout.html", "blue_hello.html")
	}
	tmpl.ExecuteTemplate(w, "layout", "")
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)
	http.HandleFunc("/process2", process2)

	server.ListenAndServe()
}
