package main

import (
	"html/template"
	"net/http"
)

func process(w http.ResponseWriter, req *http.Request) {
	tmpl, _ := template.ParseFiles("tmpl.html")
	content := `I asked <i>"What's Up"</i>`
	tmpl.Execute(w, content)
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)

	server.ListenAndServe()
}
