package main

import (
	"html/template"
	"net/http"
)

func process(w http.ResponseWriter, req *http.Request) {
	// allow XSS attacks
	w.Header().Set("X-XSS-Protection", "0")
	tmpl, _ := template.ParseFiles("tmpl.html")
	//tmpl.Execute(w, req.FormValue("comment"))
	tmpl.Execute(w, template.HTML(req.FormValue("comment")))
}

func form(w http.ResponseWriter, req *http.Request) {
	tmpl, _ := template.ParseFiles("form.html")
	tmpl.Execute(w, nil)
}
func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)
	http.HandleFunc("/form", form)

	server.ListenAndServe()
}
