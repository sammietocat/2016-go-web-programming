package main

import (
	"html/template"
	"net/http"
	"time"
)

func formatDate(t time.Time) string {
	layout := "2006-01-02"
	return t.Format(layout)
}

func process(w http.ResponseWriter, req *http.Request) {
	fm := template.FuncMap{"fmtDate": formatDate}
	tmpl := template.New("tmpl.html").Funcs(fm)
	tmpl, _ = tmpl.ParseFiles("tmpl.html")
	tmpl.Execute(w, time.Now())
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)

	server.ListenAndServe()
}
