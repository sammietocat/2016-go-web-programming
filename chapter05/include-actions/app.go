package main

import (
	"html/template"
	"net/http"
)

func process(w http.ResponseWriter, req *http.Request) {
	tmpl, _ := template.ParseFiles("t1.html", "t2.html")
	tmpl.Execute(w, "Hello World!")
}

func main() {
	server := http.Server{
		Addr: "127.0.0.1:8080",
	}

	http.HandleFunc("/process", process)

	server.ListenAndServe()
}
