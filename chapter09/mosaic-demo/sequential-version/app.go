package main

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"html/template"
	"image"
	"image/draw"
	"image/jpeg"
	"net/http"
	"os"
	"strconv"
	"time"
)

var TILES_DB map[string][3]float64

func cloneTilesDB() map[string][3]float64 {
	db := make(map[string][3]float64)
	for k, v := range TILES_DB {
		db[k] = v
	}

	return db
}

func main() {
	mux := http.NewServeMux()
	fs := http.FileServer(http.Dir("../public"))
	mux.Handle("/static/", http.StripPrefix("/static/", fs))
	mux.HandleFunc("/", upload)
	mux.HandleFunc("/mosaic", mosaic)

	server := http.Server{
		Addr:    "127.0.0.1:8080",
		Handler: mux,
	}

	TILES_DB = initTilesDB()
	fmt.Println("Mosaic server started.")
	server.ListenAndServe()
}

func upload(w http.ResponseWriter, req *http.Request) {
	tmpl, _ := template.ParseFiles("../upload.html")
	tmpl.Execute(w, nil)
}

func mosaic(w http.ResponseWriter, req *http.Request) {
	tickStart := time.Now()

	// get uploaded file and its size
	req.ParseMultipartForm(10485760)
	file, _, _ := req.FormFile("image")
	defer file.Close()
	tileSize, _ := strconv.Atoi(req.FormValue("tile_size"))

	// decode the uploaded image
	original, _, _ := image.Decode(file)
	bounds := original.Bounds()

	// save original to file for debugging
	/*
		out, err := os.Create("hello.jpg")
		if nil != err {
			fmt.Println("failed to save hello.jpg")
		}
		jpeg.Encode(out, original, nil)
		out.Close()
	*/

	mosaicImage := image.NewNRGBA(image.Rect(bounds.Min.X, bounds.Min.Y, bounds.Max.X, bounds.Max.Y))

	// clones tile database
	db := cloneTilesDB()

	// Set up source point set for each tile
	sp := image.Point{0, 0}
	// iterates through target image
	for y := bounds.Min.Y; y < bounds.Max.Y; y += tileSize {
		for x := bounds.Min.X; x < bounds.Max.X; x += tileSize {
			red, green, blue, _ := original.At(x, y).RGBA()
			color := [3]float64{float64(red), float64(green), float64(blue)}

			nearest := findNearest(color, &db)
			file, err := os.Open(nearest)
			if nil == err {
				img, _, err := image.Decode(file)
				if nil == err {
					resizedImg := resize(img, tileSize)
					tile := resizedImg.SubImage(resizedImg.Bounds())
					tileBounds := image.Rect(x, y, x+tileSize, y+tileSize)
					draw.Draw(mosaicImage, tileBounds, tile, sp, draw.Src)
				} else {
					fmt.Println("error in mosaic() > image.Decode():", err, nearest)
				}
			} else {
				fmt.Println("error in mosaic() > os.Open():", nearest)
			}
			file.Close()
		}
	}

	buf1 := new(bytes.Buffer)
	jpeg.Encode(buf1, original, nil)
	originalSrc := base64.StdEncoding.EncodeToString(buf1.Bytes())

	buf2 := new(bytes.Buffer)
	jpeg.Encode(buf2, mosaicImage, nil)
	mosaicSrc := base64.StdEncoding.EncodeToString(buf2.Bytes())

	tickEnd := time.Now()
	images := map[string]string{
		"original": originalSrc,
		"mosaic":   mosaicSrc,
		"duration": fmt.Sprintf("%v ", tickEnd.Sub(tickStart)),
	}

	tmpl, _ := template.ParseFiles("../results.html")
	tmpl.Execute(w, images)
	//tmpl.Execute(w, nil)
}
