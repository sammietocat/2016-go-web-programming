package main

import (
	"fmt"
	"image"
	"image/color"
	"io/ioutil"
	"math"
	"os"
	"sync"
)

const TILES_FOLDER = "../tiles"

type DB struct {
	mutex *sync.Mutex
	store map[string][3]float64
}

// calculate the average intensity for red, green and blue of a given image
func averageColor(img image.Image) [3]float64 {
	bounds := img.Bounds()
	redSum, greenSum, blueSum := 0.0, 0.0, 0.0
	for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
		for x := bounds.Min.X; x < bounds.Max.X; x++ {
			red, green, blue, _ := img.At(x, y).RGBA()
			redSum, greenSum, blueSum = redSum+float64(red), greenSum+float64(green), blueSum+float64(blue)
		}
	}

	area := float64(bounds.Max.X * bounds.Max.Y)
	return [3]float64{redSum / area, greenSum / area, blueSum / area}
}

// resize a given image to a specified width
func resize(in image.Image, newWidth int) image.NRGBA {
	bounds := in.Bounds()
	ratio := bounds.Dx() / newWidth
	out := image.NewNRGBA(image.Rect(bounds.Min.X/ratio, bounds.Min.Y/ratio, bounds.Max.X/ratio, bounds.Max.Y/ratio))

	for y, j := bounds.Min.Y, bounds.Min.Y; y < bounds.Max.Y; y, j = y+ratio, j+1 {
		for x, i := bounds.Min.Y, bounds.Min.Y; x < bounds.Max.Y; x, i = x+ratio, i+1 {
			red, green, blue, alpha := in.At(x, y).RGBA()
			out.SetNRGBA(i, j, color.NRGBA{uint8(red >> 8), uint8(green >> 8), uint8(blue >> 8), uint8(alpha >> 8)})
		}
	}

	return *out
}

// creates a database of the tile picture by scanning ${TILES_FOLDER} directory where the tile pictures are located
func initTilesDB() map[string][3]float64 {
	fmt.Println("Start populating tiles db...")
	db := make(map[string][3]float64)
	files, _ := ioutil.ReadDir(TILES_FOLDER)

	for _, f := range files {
		name := TILES_FOLDER + string(os.PathSeparator) + f.Name()
		file, err := os.Open(name)
		if nil == err {
			img, _, err := image.Decode(file)
			if nil == err {
				db[name] = averageColor(img)
			} else {
				fmt.Println("error in populating tiles db:", err, name)
			}
		} else {
			fmt.Println("cannot open file", name, err)
		}
		file.Close()
	}

	fmt.Println("Finished populating tiles db")
	return db
}

func square(n float64) float64 {
	return n * n
}

func distance(A [3]float64, B [3]float64) float64 {
	return math.Sqrt(square(A[0]-A[0]) + square(A[1]-A[1]) + square(A[2]-A[2]))
}

func (db *DB) findNearest(target [3]float64) string {
	var filename string
	smallest := 1000000.0

	db.mutex.Lock()
	for k, v := range db.store {
		dist := distance(target, v)
		if dist < smallest {
			filename, smallest = k, dist
		}
	}
	delete(db.store, filename)
	db.mutex.Unlock()
	return filename
}
